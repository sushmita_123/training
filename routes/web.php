<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*Route::get('/about', function () {
    return view('about');
});*/
Route::get('/about', [App\Http\Controllers\FrontendController::class, 'aboutUs'])->name('about');

Route::get('/gallery', function () {
    return view('gallery');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/category/trash', [App\Http\Controllers\CategoryController::class, 'trash'])->name('category.trash');
Route::get('/category/restore/{id}', [App\Http\Controllers\CategoryController::class, 'restore'])->name('category.restore');
Route::delete('/category/force_delete/{id}', [App\Http\Controllers\CategoryController::class, 'forceDelete'])->name('category.force_delete');

Route::get('/category/create', [App\Http\Controllers\CategoryController::class, 'create'])->name('category.create');

Route::post('/category', [App\Http\Controllers\CategoryController::class, 'store'])->name('category.store');

Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');

Route::get('/category/{id}', [App\Http\Controllers\CategoryController::class, 'show'])->name('category.show');
Route::delete('/category/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->name('category.destroy');
Route::get('/category/{id}/edit', [App\Http\Controllers\CategoryController::class, 'edit'])->name('category.edit');

Route::put('/category/{id}', [App\Http\Controllers\CategoryController::class, 'update'])->name('category.update');



Route::get('/role/create',[App\Http\Controllers\RoleController::class,'create'])->name('role.create');
Route::post('/role',[App\Http\Controllers\RoleController::class,'store'])->name('role.store');
Route::get('/role', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
Route::get('/role/{id}', [App\Http\Controllers\RoleController::class, 'show'])->name('role.show');
Route::delete('/role/{id}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('role.destroy');
Route::get('/role/{id}/edit', [App\Http\Controllers\RoleController::class, 'edit'])->name('role.edit');

Route::put('/role/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('role.update');

Route::get('/module/create', [App\Http\Controllers\ModuleController::class, 'create'])->name('module.create');
Route::post('/module', [App\Http\Controllers\ModuleController::class, 'store'])->name('module.store');
Route::get('/module', [App\Http\Controllers\ModuleController::class, 'index'])->name('module.index');
Route::get('/module/{id}', [App\Http\Controllers\ModuleController::class, 'show'])->name('module.show');
Route::delete('/module/{id}', [App\Http\Controllers\ModuleController::class, 'destroy'])->name('module.destroy');
Route::get('/module/{id}/edit', [App\Http\Controllers\ModuleController::class, 'edit'])->name('module.edit');

Route::put('/module/{id}', [App\Http\Controllers\ModuleController::class, 'update'])->name('module.update');


Route::get('/post/trash', [App\Http\Controllers\PostController::class, 'trash'])->name('post.trash');
Route::get('/post/restore/{id}', [App\Http\Controllers\PostController::class, 'restore'])->name('post.restore');
Route::delete('/post/force_delete/{id}', [App\Http\Controllers\PostController::class, 'forceDelete'])->name('post.force_delete');

//for create form
Route::get('/post/create', [App\Http\Controllers\PostController::class, 'create'])->name('post.create');
//for store
Route::post('/post', [App\Http\Controllers\PostController::class, 'store'])->name('post.store');
//for listing page
Route::get('/post', [App\Http\Controllers\PostController::class, 'index'])->name('post.index');

Route::get('/post/{id}', [App\Http\Controllers\PostController::class, 'show'])->name('post.show');

Route::delete('/post/{id}', [App\Http\Controllers\PostController::class, 'destroy'])->name('post.destroy');

Route::get('/post/{id}/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');

Route::put('/post/{id}', [App\Http\Controllers\PostController::class, 'update'])->name('post.update');

