<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['records'] = Post::orderby('created_at','desc')->get();
        return view('post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::all();
        return view('post.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation array
        $request->validate([
            'category_id' => 'required',
            'name' => 'required|max:255',
            'slug' => 'required|unique:categories|max:255',
            'image_file' =>'required'
        ]);

        if ($request->hasFile('image_file')) {
            $img = $request->file('image_file');
            $image_name = uniqid() . '_' . $img->getClientOriginalName();
            $img->move('images/post/', $image_name);
            $request->request->add(['image' => $image_name]);
        }

        //add hidden created_by field
        $request->request->add(['created_by' => Auth::user()->id]);
        //we need model to store data into database
        Post::create($request->all());
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Post::find($id);
        if (!$data['record']){
            return redirect()->route('post.index');
        }
        return view('post.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = Category::all();
        $data['record'] = Post::find($id);
        if (!$data['record']){
            return redirect()->route('post.index');
        }
        return view('post.edit',compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation array
        $request->validate([
            'name' => [
                'required',
                'max:255',
            ],
            'slug' => [
                'required',
                'max:255',
                Rule::unique('categories')->ignore($id),
            ],
        ]);

        $data['record'] = Post::find($id);

        if (!$data['record']){
            return redirect()->route('post.index');
        }
        //add hidden updated_by field
        $request->request->add(['updated_by' => Auth::user()->id]);

        //update image
        if ($request->hasFile('image_file')) {
            $img = $request->file('image_file');
            $image_name = uniqid() . '_' . $img->getClientOriginalName();
            $img->move('images/post/', $image_name);
            $request->request->add(['image' => $image_name]);
            if ($data['record']->image && file_exists('images/post/' .  $data['record']->image)){
                unlink('images/post/' .  $data['record']->image);
            }
        }

        //we need model to update data into database
        $data['record']->update($request->all());
        return redirect()->route('post.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = Post::find($id);
        if (!$data['record']){
            return redirect()->route('post.index');
        }
        //delete record
        $data['record']->delete();
        return redirect()->route('post.index');
    }

    public function trash()
    {
        $data['records'] = Post::onlyTrashed()->get();
        return view('post.trash',compact('data'));
    }

    public function restore($id)
    {
        $data['records'] = Post::onlyTrashed()->where('id',$id)->restore();
        return redirect()->route('post.index');
    }

    public function forceDelete($id)
    {
        $data = Post::onlyTrashed()->where('id',$id)->get();
        if ($data[0]->image && file_exists('images/post/' .  $data[0]->image)){
            unlink('images/post/' .  $data[0]->image);
        }
        $data[0]->forceDelete();
        return redirect()->route('post.index');
    }
}
