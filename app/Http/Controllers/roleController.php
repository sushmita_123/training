<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from database using model
        // $data['records'] = Category::all();
        //fetch all data ascending order by rank
        $data['records'] = Role::orderby('created_by')->get();
        //$data['records'] = Category::order by('created_by')->paging(2);
        return view('role.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
        Role::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Role::find($id);
        if (!$data['record']){
            return redirect()->route('role.index');
        }
        return view('role.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['record'] = Role::find($id);
        if (!$data['record']){
            return redirect()->route('role.index');
        }
        return view('role.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['record'] = Role::find($id);
        if (!$data['record']){
            return redirect()->route('role.index');
        }
        //add hidden updated_by field
        $request->request->add(['updated_by' => Auth::user()->id]);

        //we need model to update data into database
        $data['record']->update($request->all());
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = Role::find($id);
        if (!$data['record']){
            return redirect()->route('role.index');
        }
        $data['record']->delete();
        return redirect()->route('role.index');
    }
}
