@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Module Create') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{route('module.store')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="route">Route</label>
                                <input type="text" id="route" name="route" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="boolean" id="status" name="status" class="form-control">
                            </div>

                            <div class="form-group">
                                <input type="submit" name="btnSave" value="Save" class="btn btn-success">
                                <input type="reset" class="btn btn-danger">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
