@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Role Edit') }}</div>
                    <a class="btn btn-dark" href="{{route('role.index')}}">List</a>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{route('role.update',$data['record']->id)}}" method="post">
                            <input type="hidden" name="_method" value="PUT">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{$data['record']->name}}">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="boolean" id="status" name="status" class="form-control"value="{{$data['record']->status}}">
                            </div>

                            <div class="form-group">
                                <input type="submit" name="btnSave" value="Update Category" class="btn btn-success">
                                <input type="reset" class="btn btn-danger">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
