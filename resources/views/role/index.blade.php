@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Role List') }}
                        <a class="btn btn-dark" href="{{route('role.create')}}">Create</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['records'] as $record)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$record->name}}</td>
                                    <td>{{$record->status}}</td>
                                    <td>{{$record->created_at}}</td>
                                    <td>
                                        <a href="{{route('role.show',$record->id)}}" class="btn btn-success">View</a>
                                        <a href="{{route('role.edit',$record->id)}}" class="btn btn-primary">Update</a>

                                        <form action="{{route('role.destroy',$record->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" name="Delete" value="Delete" class="btn btn-danger">
                                        </form>
                                        Edit/Delete/View</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
