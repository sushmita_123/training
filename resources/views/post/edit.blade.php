@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Post Edit') }}</div>
                    <a class="btn btn-dark" href="{{route('post.index')}}">List</a>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('post.update',$data['record']->id)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                            @csrf
                                <div class="form-group">
                                    <label for="category_id">Category</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach($data['categories'] as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{$data['record']->name}}">
                            @error('name')
                                <span class="text-danger" >{{$message}}</span>
                            @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" id="slug" name="slug" class="form-control"value="{{$data['record']->slug}}">
                                @error('slug')
                                <span class="text-danger" >{{$message}}</span>
                                @enderror
                            </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" id="image" name="image_file" class="form-control">
                                    @error('image_file')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea name="short_description" class="form-control" id="short_description" cols="30" rows="3">{{$data['record']->short_description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control" id="description" cols="30" rows="3">{{$data['record']->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" name="btnSave" value="Update Post" class="btn btn-success">
                                <input type="reset" class="btn btn-danger">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
