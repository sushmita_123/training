@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Post Trash') }}
                        <a class="btn btn-success" href="{{route('post.create')}}">Create</a>
                        <a class="btn btn-danger" href="{{route('post.trash')}}">Trash</a>

                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['records'] as $record)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$record->name}}</td>
                                    <td>{{$record->slug}}</td>
                                    <td>{{$record->image}}</td>
                                    <td>{{$record->created_at}}</td>
                                    <td>
                                        <a href="{{route('post.restore',$record->id)}}" class="btn btn-info">Restore</a>
                                        <form action="{{route('post.force_delete',$record->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" name="Force Delete" value="Delete" class="btn btn-danger">
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
